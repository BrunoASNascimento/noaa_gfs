import pandas_gbq
from datetime import datetime, timedelta


directory = '/var/scripts/noaa_gfs/'
# directory = './'  # !Local test

date = datetime.utcnow()-timedelta(days=0)
print(f'Date create {date}')
date_str = date.strftime('%Y%m%d')

hr_list = ['00']

sql = ("""
        SELECT
        DISTINCT 
            latitude_gfs,
            longitude_gfs
        FROM
            `pluvion-tech.stations.all_stations`
        ORDER BY
            1 DESC,
            2
  """)
df = pandas_gbq.read_gbq(sql, project_id='pluvion-tech')
print(f'Get {df.shape[0]} points')

line_list = []
for index, row in df.iterrows():
    line = ('       /opt/wgrib2/grib2/wgrib2/wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola'+'  '+str((row['longitude_gfs']))+':1:1'+'  '+str((row['latitude_gfs']))+':1:1 '+str(
        index+1).zfill(3)+'_gfs_${date}${hr}f${i}.grb grib\n          /opt/wgrib2/grib2/wgrib2/wgrib2 '
        + str(index+1).zfill(3)+'_gfs_${date}${hr}f${i}.grb -csv '+str(index+1).zfill(3)+'_gfs_${date}${hr}f${i}.csv &\n ')
    line_list.append(line)

line_str = "\n".join(line_list)

for hr in hr_list:
    print(f'Creater document to {hr}hr')
    with open(directory+f'get_noaa_{hr}.sh', 'w') as get_noaa:
        get_noaa.write(
            """
#!/bin/bash
#
#define URL
#


cd /var/data/gfs

fhr=0p25
date=$(date +'%Y%m%d')

hr="""+hr+"""
echo "$date"
for i in {001..120..1}; do
    URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
    echo "$URL"
    #download file
    # curl "$URL" -o gfs_${date}${hr}f${i}.grb
    wget -o wgetlog --output-document=gfs_${date}${hr}f${i}.grb --no-proxy "$URL" -q  --show-progress
    \n
        """ + line_str +
            """
    # add a sleep to prevent a denial of service in case of missing file
    sleep 1
    python3 /var/scripts/noaa_gfs/parser_csv.py &
done

python3 /var/scripts/noaa_gfs/parser_csv.py &


for i in {123..240..3}; do
    URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
    echo "$URL"
    #download file
    # curl "$URL" -o gfs_${date}${hr}f${i}.grb
    wget -o wgetlog --output-document=gfs_${date}${hr}f${i}.grb --no-proxy "$URL" -q  --show-progress
    \n
""" + line_str +
            """
    # add a sleep to prevent a denial of service in case of missing file
    sleep 1
    python3 /var/scripts/noaa_gfs/parser_csv.py &
done

python3 /var/scripts/noaa_gfs/parser_csv.py &

for i in {252..384..12}; 
    do
        URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
        echo "$URL"
        #download file
        # curl "$URL" -o gfs_${date}${hr}f${i}.grb
        wget -o wgetlog --output-document=gfs_${date}${hr}f${i}.grb --no-proxy "$URL" -q  --show-progress
        \n
""" + line_str +
            """
    # add a sleep to prevent a denial of service in case of missing file
    sleep 1
    python3 /var/scripts/noaa_gfs/parser_csv.py &
done

python3 /var/scripts/noaa_gfs/parser_csv.py &

# tar -czf gfs_${date}${hr}.tar.gz *.grb
rm *.grb
# #gsutil cp gfs_${date}${hr}.tar.gz gs://pluvion_forecasts/grib_gfs
# gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 gfs_${date}${hr}.tar.gz
# rm gfs_${date}${hr}.tar.gz
"""
        )
