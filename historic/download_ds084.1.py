#!/usr/bin/env python
#################################################################
# Python Script to retrieve 21 online Data files of 'ds084.1',
# total 14.32G. This script uses 'requests' to download data.
#
# Highlight this script by Select All, Copy and Paste it into a file;
# make the file executable and run it on command line.
#
# You need pass in your password as a parameter to execute
# this script; or you can set an environment variable RDAPSWD
# if your Operating System supports it.
#
# Contact rpconroy@ucar.edu (Riley Conroy) for further assistance.
#################################################################


import sys
import os
import requests


def check_file_status(filepath, filesize):
    sys.stdout.write('\r')
    sys.stdout.flush()
    size = int(os.stat(filepath).st_size)
    percent_complete = (size/filesize)*100
    sys.stdout.write('%.3f %s' % (percent_complete, '% Completed'))
    sys.stdout.flush()


# Try to get password
if len(sys.argv) < 2 and not 'RDAPSWD' in os.environ:
    try:
        import getpass
        input = getpass.getpass
    except:
        try:
            input = raw_input
        except:
            pass
    pswd = "pluvion2018"
else:
    try:
        pswd = sys.argv[1]
    except:
        pswd = os.environ['RDAPSWD']

url = 'https://rda.ucar.edu/cgi-bin/login'
values = {'email': 'tech@pluvion.com.br', 'passwd': pswd, 'action': 'login'}
# Authenticate
ret = requests.post(url, data=values)
if ret.status_code != 200:
    print('Bad Authentication')
    print(ret.text)
    exit(1)
dspath = 'https://rda.ucar.edu/dsrqst/TECHNOLOGIES431064/'
filelist = [
    'TarFiles/gfs.0p25.2019121600.f384-2019122712.f144.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2019122712.f147-2019123018.f126.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2019123018.f129-2020010206.f147.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020010206.f150-2020010418.f108.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020010418.f111-2020010706.f069.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020010706.f072-2020010918.f030.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020010918.f033-2020011200.f372.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020011200.f384-2020011412.f237.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020011412.f240-2020011700.f198.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020011700.f201-2020011912.f159.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020011912.f162-2020012200.f120.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020012200.f123-2020012412.f081.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020012412.f084-2020012700.f042.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020012700.f045-2020012912.f003.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020012912.f006-2020013118.f252.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020013118.f264-2020020306.f204.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020020306.f207-2020020518.f165.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020020518.f168-2020020806.f126.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020020806.f129-2020021018.f087.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020021018.f090-2020021306.f048.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020021306.f051-2020021518.f009.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020021518.f012-2020021800.f276.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020021800.f288-2020022012.f210.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020022012.f213-2020022300.f171.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020022300.f174-2020022512.f132.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020022512.f135-2020022800.f093.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020022800.f096-2020030112.f054.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020030112.f057-2020030400.f015.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020030400.f018-2020030606.f300.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020030606.f312-2020030818.f216.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020030818.f219-2020031106.f177.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020031106.f180-2020031318.f138.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020031318.f141-2020031606.f099.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020031606.f102-2020031818.f060.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020031818.f063-2020032106.f021.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020032106.f024-2020032312.f324.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020032312.f336-2020032600.f222.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020032600.f225-2020032812.f183.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020032812.f186-2020033100.f144.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020033100.f147-2020040212.f105.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020040212.f108-2020040500.f066.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020040500.f069-2020040712.f027.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020040712.f030-2020040918.f348.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020040918.f360-2020041206.f228.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020041206.f231-2020041418.f189.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020041418.f192-2020041706.f150.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020041706.f153-2020041918.f111.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020041918.f114-2020042206.f072.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020042206.f075-2020042418.f033.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020042418.f036-2020042700.f372.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020042700.f384-2020042912.f234.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020042912.f237-2020050200.f195.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020050200.f198-2020050412.f156.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020050412.f159-2020050700.f117.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020050700.f120-2020050912.f078.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020050912.f081-2020051200.f039.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020051200.f042-2020051412.f009.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020051412.f012-2020051618.f276.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020051618.f288-2020051906.f210.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020051906.f213-2020052118.f171.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020052118.f174-2020052406.f132.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020052406.f135-2020052618.f093.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020052618.f096-2020052906.f054.grib2.spasub.technologies431064.tar',
    'TarFiles/gfs.0p25.2020052906.f057.grib2-t06z.pgrb2.0p25.f144.spasub.technologies431064.tar'
]
name_directory = '/var/data/gfs/historic/'
for file in filelist:
    filename = dspath+file
    file_base = os.path.basename(name_directory+file)
    print('Downloading', file_base)
    req = requests.get(filename, cookies=ret.cookies,
                       allow_redirects=True, stream=True)
    filesize = int(req.headers['Content-length'])
    with open(name_directory+file_base, 'wb') as outfile:
        chunk_size = 1048576
        for chunk in req.iter_content(chunk_size=chunk_size):
            outfile.write(chunk)
            if chunk_size < filesize:
                check_file_status(name_directory+file_base, filesize)
    check_file_status(name_directory+file_base, filesize)
    print()
