#!/bin/bash
#
cd /var/data/gfs/historic

for i in *.tar
do
    tar -xvf ${i}
    rm ${i}
    for j in *.technologies431064
    do
        echo "-------------------------   "${j}"   -------------------------"
        /opt/wgrib2/grib2/wgrib2/wgrib2 -match "surface" ./${j} -csv /var/data/gfs/historic_${j}.csv
        # wgrib2 ${j} -match "surface" -set_grib_type same -new_grid_winds earth -new_grid latlon  -55:40:0.25  -24:34:0.25 urgente_${j}.grb
        # wgrib2 -match "surface" ./urgente_${j}.grb -csv /var/data/gfs/urgente_${j}.csv
        rm ${j}
    done
    
    # python3 /var/scripts/noaa_gfs/main_normal.py  &
    # python3 /var/scripts/noaa_gfs/main_reverse.py
    
    # /var/scripts/noaa_gfs/upload_backup.sh
    
    
done