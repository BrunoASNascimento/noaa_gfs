import requests
from datetime import datetime, timedelta

fhr = "0p25"
hr = str(0).zfill(2)


def download():

    date = str(20191023)
    i = str(1).zfill(3)

    url = ("https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t" +
           hr
           +
           "z.pgrb2."
           +
           fhr
           +
           ".f"+i+"&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs." +
           date
           +
           "%2F"+hr
           )
    r = requests.get(url, allow_redirects=True)
    open('google.grb', 'wb')
    print(url)


download()
