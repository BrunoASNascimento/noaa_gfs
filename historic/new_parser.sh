#!/bin/bash
#
cd /var/data/gfs/historic

for i in *.tar
do
    tar -xvf ${i}
    rm ${i}
    rm *6.f*.grib2.spasub.technologies431064
    rm *12.f*.grib2.spasub.technologies431064
    rm *18.f*.grib2.spasub.technologies431064
    rm *20200415*.grib2.spasub.technologies431064
    rm *20200416*.grib2.spasub.technologies431064
    rm *20200417*.grib2.spasub.technologies431064
    rm *20200418*.grib2.spasub.technologies431064
    rm *20200419*.grib2.spasub.technologies431064
    rm *20200420*.grib2.spasub.technologies431064
    rm *20200423*.grib2.spasub.technologies431064
    rm *20200428*.grib2.spasub.technologies431064
    rm *20200429*.grib2.spasub.technologies431064
    rm *20200430*.grib2.spasub.technologies431064
    rm *20200501*.grib2.spasub.technologies431064
    rm *20200502*.grib2.spasub.technologies431064
    rm *20200506*.grib2.spasub.technologies431064
    rm *20200507*.grib2.spasub.technologies431064
    rm *20200515*.grib2.spasub.technologies431064
    rm *20200516*.grib2.spasub.technologies431064
    rm *20200517*.grib2.spasub.technologies431064
    rm *20200518*.grib2.spasub.technologies431064
    rm *20200519*.grib2.spasub.technologies431064
    rm *20200520*.grib2.spasub.technologies431064
    rm *20200521*.grib2.spasub.technologies431064
    rm *20200522*.grib2.spasub.technologies431064
    rm *20200523*.grib2.spasub.technologies431064
    rm *20200524*.grib2.spasub.technologies431064
    rm *20200525*.grib2.spasub.technologies431064
    rm *20200526*.grib2.spasub.technologies431064
    rm *20200527*.grib2.spasub.technologies431064
    rm *20200528*.grib2.spasub.technologies431064
    rm *20200529*.grib2.spasub.technologies431064
    rm *20200530*.grib2.spasub.technologies431064
    rm *20200531*.grib2.spasub.technologies431064
    rm *20200601*.grib2.spasub.technologies431064
    rm *20200602*.grib2.spasub.technologies431064
    rm *20200603*.grib2.spasub.technologies431064
    rm *20200604*.grib2.spasub.technologies431064
    for j in *.technologies431064
    do
        echo "-------------------------   "${j}"   -------------------------"
        
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -52.0:1:1  -3.25:1:1 001_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 001_${j} -csv 001_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -51.75:1:1  -3.25:1:1 002_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 002_${j} -csv 002_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -52.0:1:1  -3.5:1:1 003_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 003_${j} -csv 003_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -35.75:1:1  -9.75:1:1 004_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 004_${j} -csv 004_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -54.5:1:1  -11.75:1:1 005_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 005_${j} -csv 005_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -38.5:1:1  -13.0:1:1 006_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 006_${j} -csv 006_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -54.75:1:1  -16.75:1:1 007_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 007_${j} -csv 007_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -54.25:1:1  -17.25:1:1 008_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 008_${j} -csv 008_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -53.25:1:1  -17.25:1:1 009_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 009_${j} -csv 009_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -53.25:1:1  -17.75:1:1 010_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 010_${j} -csv 010_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -51.25:1:1  -17.75:1:1 011_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 011_${j} -csv 011_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.25:1:1  -17.75:1:1 012_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 012_${j} -csv 012_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.25:1:1  -18.5:1:1 013_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 013_${j} -csv 013_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.25:1:1  -18.75:1:1 014_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 014_${j} -csv 014_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.0:1:1  -18.75:1:1 015_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 015_${j} -csv 015_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.75:1:1  -19.5:1:1 016_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 016_${j} -csv 016_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -51.75:1:1  -19.75:1:1 017_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 017_${j} -csv 017_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -51.0:1:1  -20.0:1:1 018_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 018_${j} -csv 018_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -44.0:1:1  -20.0:1:1 019_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 019_${j} -csv 019_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -50.5:1:1  -20.25:1:1 020_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 020_${j} -csv 020_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -50.25:1:1  -20.25:1:1 021_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 021_${j} -csv 021_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -50.0:1:1  -20.5:1:1 022_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 022_${j} -csv 022_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -49.5:1:1  -20.75:1:1 023_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 023_${j} -csv 023_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -49.0:1:1  -21.25:1:1 024_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 024_${j} -csv 024_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.5:1:1  -21.25:1:1 025_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 025_${j} -csv 025_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.0:1:1  -21.25:1:1 026_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 026_${j} -csv 026_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.75:1:1  -21.5:1:1 027_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 027_${j} -csv 027_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.25:1:1  -21.75:1:1 028_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 028_${j} -csv 028_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.0:1:1  -21.75:1:1 029_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 029_${j} -csv 029_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.0:1:1  -22.0:1:1 030_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 030_${j} -csv 030_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -49.0:1:1  -22.25:1:1 031_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 031_${j} -csv 031_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -48.0:1:1  -22.25:1:1 032_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 032_${j} -csv 032_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.75:1:1  -22.25:1:1 033_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 033_${j} -csv 033_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.5:1:1  -22.5:1:1 034_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 034_${j} -csv 034_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -50.25:1:1  -22.75:1:1 035_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 035_${j} -csv 035_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.25:1:1  -22.75:1:1 036_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 036_${j} -csv 036_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.0:1:1  -22.75:1:1 037_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 037_${j} -csv 037_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.25:1:1  -23.0:1:1 038_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 038_${j} -csv 038_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.0:1:1  -23.0:1:1 039_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 039_${j} -csv 039_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.75:1:1  -23.0:1:1 040_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 040_${j} -csv 040_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.0:1:1  -23.0:1:1 041_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 041_${j} -csv 041_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -43.25:1:1  -23.0:1:1 042_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 042_${j} -csv 042_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.25:1:1  -23.25:1:1 043_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 043_${j} -csv 043_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -45.75:1:1  -23.25:1:1 044_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 044_${j} -csv 044_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.25:1:1  -23.5:1:1 045_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 045_${j} -csv 045_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.75:1:1  -23.5:1:1 046_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 046_${j} -csv 046_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.5:1:1  -23.5:1:1 047_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 047_${j} -csv 047_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.25:1:1  -23.5:1:1 048_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 048_${j} -csv 048_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -45.5:1:1  -23.5:1:1 049_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 049_${j} -csv 049_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.25:1:1  -23.75:1:1 050_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 050_${j} -csv 050_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.0:1:1  -23.75:1:1 051_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 051_${j} -csv 051_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.75:1:1  -23.75:1:1 052_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 052_${j} -csv 052_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.5:1:1  -23.75:1:1 053_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 053_${j} -csv 053_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.25:1:1  -24.0:1:1 054_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 054_${j} -csv 054_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -47.0:1:1  -24.0:1:1 055_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 055_${j} -csv 055_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.75:1:1  -24.0:1:1 056_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 056_${j} -csv 056_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.5:1:1  -24.0:1:1 057_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 057_${j} -csv 057_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -46.25:1:1  -24.0:1:1 058_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 058_${j} -csv 058_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -54.5:1:1  -25.5:1:1 059_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 059_${j} -csv 059_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -51.5:1:1  -25.5:1:1 060_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 060_${j} -csv 060_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -49.25:1:1  -25.5:1:1 061_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 061_${j} -csv 061_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -43.0:1:1  -27.0:1:1 062_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 062_${j} -csv 062_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -51.25:1:1  -27.25:1:1 063_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 063_${j} -csv 063_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -52.5:1:1  -28.25:1:1 064_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 064_${j} -csv 064_${j}.csv &
        
        /opt/wgrib2/grib2/wgrib2/wgrib2 ${j} -match "surface" -lola  -51.25:1:1  -29.75:1:1 065_${j} grib
        /opt/wgrib2/grib2/wgrib2/wgrib2 065_${j} -csv 065_${j}.csv &
        
        # add a sleep to prevent a denial of service in case of missing file
        sleep 1
        
        mv /var/data/gfs/historic/*.csv /var/data/gfs
        python3 /var/scripts/noaa_gfs/parser_csv.py &
        
        
        rm ${j}
    done
    rm 0*
    rm *.technologies431064
    
    
    
done