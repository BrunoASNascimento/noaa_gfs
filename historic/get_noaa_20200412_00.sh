
    #!/bin/bash
    #
    #define URL
    #


    cd /var/data/gfs

    fhr=0p25
    date=20200412

    hr=00
    for i in {001..120..1}; do
        URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
        echo "$URL"
        #download file
        curl "$URL" -o gfs_${date}${hr}f${i}.grb

                   wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.0:1:1  -3.25:1:1 001_gfs_${date}${hr}f${i}.grb grib
          wgrib2 001_gfs_${date}${hr}f${i}.grb -csv 001_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.75:1:1  -3.25:1:1 002_gfs_${date}${hr}f${i}.grb grib
          wgrib2 002_gfs_${date}${hr}f${i}.grb -csv 002_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.0:1:1  -3.5:1:1 003_gfs_${date}${hr}f${i}.grb grib
          wgrib2 003_gfs_${date}${hr}f${i}.grb -csv 003_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -35.75:1:1  -9.75:1:1 004_gfs_${date}${hr}f${i}.grb grib
          wgrib2 004_gfs_${date}${hr}f${i}.grb -csv 004_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.5:1:1  -11.75:1:1 005_gfs_${date}${hr}f${i}.grb grib
          wgrib2 005_gfs_${date}${hr}f${i}.grb -csv 005_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -38.5:1:1  -13.0:1:1 006_gfs_${date}${hr}f${i}.grb grib
          wgrib2 006_gfs_${date}${hr}f${i}.grb -csv 006_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.75:1:1  -16.75:1:1 007_gfs_${date}${hr}f${i}.grb grib
          wgrib2 007_gfs_${date}${hr}f${i}.grb -csv 007_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.25:1:1  -17.25:1:1 008_gfs_${date}${hr}f${i}.grb grib
          wgrib2 008_gfs_${date}${hr}f${i}.grb -csv 008_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -53.25:1:1  -17.25:1:1 009_gfs_${date}${hr}f${i}.grb grib
          wgrib2 009_gfs_${date}${hr}f${i}.grb -csv 009_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -53.25:1:1  -17.75:1:1 010_gfs_${date}${hr}f${i}.grb grib
          wgrib2 010_gfs_${date}${hr}f${i}.grb -csv 010_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -17.75:1:1 011_gfs_${date}${hr}f${i}.grb grib
          wgrib2 011_gfs_${date}${hr}f${i}.grb -csv 011_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -17.75:1:1 012_gfs_${date}${hr}f${i}.grb grib
          wgrib2 012_gfs_${date}${hr}f${i}.grb -csv 012_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -18.5:1:1 013_gfs_${date}${hr}f${i}.grb grib
          wgrib2 013_gfs_${date}${hr}f${i}.grb -csv 013_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -18.75:1:1 014_gfs_${date}${hr}f${i}.grb grib
          wgrib2 014_gfs_${date}${hr}f${i}.grb -csv 014_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -18.75:1:1 015_gfs_${date}${hr}f${i}.grb grib
          wgrib2 015_gfs_${date}${hr}f${i}.grb -csv 015_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -19.5:1:1 016_gfs_${date}${hr}f${i}.grb grib
          wgrib2 016_gfs_${date}${hr}f${i}.grb -csv 016_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.75:1:1  -19.75:1:1 017_gfs_${date}${hr}f${i}.grb grib
          wgrib2 017_gfs_${date}${hr}f${i}.grb -csv 017_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.0:1:1  -20.0:1:1 018_gfs_${date}${hr}f${i}.grb grib
          wgrib2 018_gfs_${date}${hr}f${i}.grb -csv 018_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -44.0:1:1  -20.0:1:1 019_gfs_${date}${hr}f${i}.grb grib
          wgrib2 019_gfs_${date}${hr}f${i}.grb -csv 019_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.5:1:1  -20.25:1:1 020_gfs_${date}${hr}f${i}.grb grib
          wgrib2 020_gfs_${date}${hr}f${i}.grb -csv 020_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.25:1:1  -20.25:1:1 021_gfs_${date}${hr}f${i}.grb grib
          wgrib2 021_gfs_${date}${hr}f${i}.grb -csv 021_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.0:1:1  -20.5:1:1 022_gfs_${date}${hr}f${i}.grb grib
          wgrib2 022_gfs_${date}${hr}f${i}.grb -csv 022_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.5:1:1  -20.75:1:1 023_gfs_${date}${hr}f${i}.grb grib
          wgrib2 023_gfs_${date}${hr}f${i}.grb -csv 023_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.0:1:1  -21.25:1:1 024_gfs_${date}${hr}f${i}.grb grib
          wgrib2 024_gfs_${date}${hr}f${i}.grb -csv 024_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.5:1:1  -21.25:1:1 025_gfs_${date}${hr}f${i}.grb grib
          wgrib2 025_gfs_${date}${hr}f${i}.grb -csv 025_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -21.25:1:1 026_gfs_${date}${hr}f${i}.grb grib
          wgrib2 026_gfs_${date}${hr}f${i}.grb -csv 026_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.75:1:1  -21.5:1:1 027_gfs_${date}${hr}f${i}.grb grib
          wgrib2 027_gfs_${date}${hr}f${i}.grb -csv 027_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -21.75:1:1 028_gfs_${date}${hr}f${i}.grb grib
          wgrib2 028_gfs_${date}${hr}f${i}.grb -csv 028_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -21.75:1:1 029_gfs_${date}${hr}f${i}.grb grib
          wgrib2 029_gfs_${date}${hr}f${i}.grb -csv 029_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -22.0:1:1 030_gfs_${date}${hr}f${i}.grb grib
          wgrib2 030_gfs_${date}${hr}f${i}.grb -csv 030_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.0:1:1  -22.25:1:1 031_gfs_${date}${hr}f${i}.grb grib
          wgrib2 031_gfs_${date}${hr}f${i}.grb -csv 031_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -22.25:1:1 032_gfs_${date}${hr}f${i}.grb grib
          wgrib2 032_gfs_${date}${hr}f${i}.grb -csv 032_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.75:1:1  -22.25:1:1 033_gfs_${date}${hr}f${i}.grb grib
          wgrib2 033_gfs_${date}${hr}f${i}.grb -csv 033_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.5:1:1  -22.5:1:1 034_gfs_${date}${hr}f${i}.grb grib
          wgrib2 034_gfs_${date}${hr}f${i}.grb -csv 034_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.25:1:1  -22.75:1:1 035_gfs_${date}${hr}f${i}.grb grib
          wgrib2 035_gfs_${date}${hr}f${i}.grb -csv 035_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -22.75:1:1 036_gfs_${date}${hr}f${i}.grb grib
          wgrib2 036_gfs_${date}${hr}f${i}.grb -csv 036_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -22.75:1:1 037_gfs_${date}${hr}f${i}.grb grib
          wgrib2 037_gfs_${date}${hr}f${i}.grb -csv 037_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.0:1:1 038_gfs_${date}${hr}f${i}.grb grib
          wgrib2 038_gfs_${date}${hr}f${i}.grb -csv 038_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -23.0:1:1 039_gfs_${date}${hr}f${i}.grb grib
          wgrib2 039_gfs_${date}${hr}f${i}.grb -csv 039_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.0:1:1 040_gfs_${date}${hr}f${i}.grb grib
          wgrib2 040_gfs_${date}${hr}f${i}.grb -csv 040_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.0:1:1  -23.0:1:1 041_gfs_${date}${hr}f${i}.grb grib
          wgrib2 041_gfs_${date}${hr}f${i}.grb -csv 041_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -43.25:1:1  -23.0:1:1 042_gfs_${date}${hr}f${i}.grb grib
          wgrib2 042_gfs_${date}${hr}f${i}.grb -csv 042_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.25:1:1 043_gfs_${date}${hr}f${i}.grb grib
          wgrib2 043_gfs_${date}${hr}f${i}.grb -csv 043_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -45.75:1:1  -23.25:1:1 044_gfs_${date}${hr}f${i}.grb grib
          wgrib2 044_gfs_${date}${hr}f${i}.grb -csv 044_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.5:1:1 045_gfs_${date}${hr}f${i}.grb grib
          wgrib2 045_gfs_${date}${hr}f${i}.grb -csv 045_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.5:1:1 046_gfs_${date}${hr}f${i}.grb grib
          wgrib2 046_gfs_${date}${hr}f${i}.grb -csv 046_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -23.5:1:1 047_gfs_${date}${hr}f${i}.grb grib
          wgrib2 047_gfs_${date}${hr}f${i}.grb -csv 047_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.25:1:1  -23.5:1:1 048_gfs_${date}${hr}f${i}.grb grib
          wgrib2 048_gfs_${date}${hr}f${i}.grb -csv 048_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -45.5:1:1  -23.5:1:1 049_gfs_${date}${hr}f${i}.grb grib
          wgrib2 049_gfs_${date}${hr}f${i}.grb -csv 049_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.75:1:1 050_gfs_${date}${hr}f${i}.grb grib
          wgrib2 050_gfs_${date}${hr}f${i}.grb -csv 050_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -23.75:1:1 051_gfs_${date}${hr}f${i}.grb grib
          wgrib2 051_gfs_${date}${hr}f${i}.grb -csv 051_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.75:1:1 052_gfs_${date}${hr}f${i}.grb grib
          wgrib2 052_gfs_${date}${hr}f${i}.grb -csv 052_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -23.75:1:1 053_gfs_${date}${hr}f${i}.grb grib
          wgrib2 053_gfs_${date}${hr}f${i}.grb -csv 053_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -24.0:1:1 054_gfs_${date}${hr}f${i}.grb grib
          wgrib2 054_gfs_${date}${hr}f${i}.grb -csv 054_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -24.0:1:1 055_gfs_${date}${hr}f${i}.grb grib
          wgrib2 055_gfs_${date}${hr}f${i}.grb -csv 055_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -24.0:1:1 056_gfs_${date}${hr}f${i}.grb grib
          wgrib2 056_gfs_${date}${hr}f${i}.grb -csv 056_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -24.0:1:1 057_gfs_${date}${hr}f${i}.grb grib
          wgrib2 057_gfs_${date}${hr}f${i}.grb -csv 057_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.25:1:1  -24.0:1:1 058_gfs_${date}${hr}f${i}.grb grib
          wgrib2 058_gfs_${date}${hr}f${i}.grb -csv 058_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.5:1:1  -25.5:1:1 059_gfs_${date}${hr}f${i}.grb grib
          wgrib2 059_gfs_${date}${hr}f${i}.grb -csv 059_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.5:1:1  -25.5:1:1 060_gfs_${date}${hr}f${i}.grb grib
          wgrib2 060_gfs_${date}${hr}f${i}.grb -csv 060_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.25:1:1  -25.5:1:1 061_gfs_${date}${hr}f${i}.grb grib
          wgrib2 061_gfs_${date}${hr}f${i}.grb -csv 061_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -43.0:1:1  -27.0:1:1 062_gfs_${date}${hr}f${i}.grb grib
          wgrib2 062_gfs_${date}${hr}f${i}.grb -csv 062_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -27.25:1:1 063_gfs_${date}${hr}f${i}.grb grib
          wgrib2 063_gfs_${date}${hr}f${i}.grb -csv 063_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.5:1:1  -28.25:1:1 064_gfs_${date}${hr}f${i}.grb grib
          wgrib2 064_gfs_${date}${hr}f${i}.grb -csv 064_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -29.75:1:1 065_gfs_${date}${hr}f${i}.grb grib
          wgrib2 065_gfs_${date}${hr}f${i}.grb -csv 065_gfs_${date}${hr}f${i}.csv &
 
        # add a sleep to prevent a denial of service in case of missing file
        sleep 1
        python3 /var/scripts/noaa_gfs/parser_csv.py &
    done

    


    for i in {123..240..3}; do
        URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
        echo "$URL"
        #download file
        curl "$URL" -o gfs_${date}${hr}f${i}.grb

           wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.0:1:1  -3.25:1:1 001_gfs_${date}${hr}f${i}.grb grib
          wgrib2 001_gfs_${date}${hr}f${i}.grb -csv 001_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.75:1:1  -3.25:1:1 002_gfs_${date}${hr}f${i}.grb grib
          wgrib2 002_gfs_${date}${hr}f${i}.grb -csv 002_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.0:1:1  -3.5:1:1 003_gfs_${date}${hr}f${i}.grb grib
          wgrib2 003_gfs_${date}${hr}f${i}.grb -csv 003_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -35.75:1:1  -9.75:1:1 004_gfs_${date}${hr}f${i}.grb grib
          wgrib2 004_gfs_${date}${hr}f${i}.grb -csv 004_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.5:1:1  -11.75:1:1 005_gfs_${date}${hr}f${i}.grb grib
          wgrib2 005_gfs_${date}${hr}f${i}.grb -csv 005_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -38.5:1:1  -13.0:1:1 006_gfs_${date}${hr}f${i}.grb grib
          wgrib2 006_gfs_${date}${hr}f${i}.grb -csv 006_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.75:1:1  -16.75:1:1 007_gfs_${date}${hr}f${i}.grb grib
          wgrib2 007_gfs_${date}${hr}f${i}.grb -csv 007_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.25:1:1  -17.25:1:1 008_gfs_${date}${hr}f${i}.grb grib
          wgrib2 008_gfs_${date}${hr}f${i}.grb -csv 008_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -53.25:1:1  -17.25:1:1 009_gfs_${date}${hr}f${i}.grb grib
          wgrib2 009_gfs_${date}${hr}f${i}.grb -csv 009_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -53.25:1:1  -17.75:1:1 010_gfs_${date}${hr}f${i}.grb grib
          wgrib2 010_gfs_${date}${hr}f${i}.grb -csv 010_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -17.75:1:1 011_gfs_${date}${hr}f${i}.grb grib
          wgrib2 011_gfs_${date}${hr}f${i}.grb -csv 011_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -17.75:1:1 012_gfs_${date}${hr}f${i}.grb grib
          wgrib2 012_gfs_${date}${hr}f${i}.grb -csv 012_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -18.5:1:1 013_gfs_${date}${hr}f${i}.grb grib
          wgrib2 013_gfs_${date}${hr}f${i}.grb -csv 013_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -18.75:1:1 014_gfs_${date}${hr}f${i}.grb grib
          wgrib2 014_gfs_${date}${hr}f${i}.grb -csv 014_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -18.75:1:1 015_gfs_${date}${hr}f${i}.grb grib
          wgrib2 015_gfs_${date}${hr}f${i}.grb -csv 015_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -19.5:1:1 016_gfs_${date}${hr}f${i}.grb grib
          wgrib2 016_gfs_${date}${hr}f${i}.grb -csv 016_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.75:1:1  -19.75:1:1 017_gfs_${date}${hr}f${i}.grb grib
          wgrib2 017_gfs_${date}${hr}f${i}.grb -csv 017_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.0:1:1  -20.0:1:1 018_gfs_${date}${hr}f${i}.grb grib
          wgrib2 018_gfs_${date}${hr}f${i}.grb -csv 018_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -44.0:1:1  -20.0:1:1 019_gfs_${date}${hr}f${i}.grb grib
          wgrib2 019_gfs_${date}${hr}f${i}.grb -csv 019_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.5:1:1  -20.25:1:1 020_gfs_${date}${hr}f${i}.grb grib
          wgrib2 020_gfs_${date}${hr}f${i}.grb -csv 020_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.25:1:1  -20.25:1:1 021_gfs_${date}${hr}f${i}.grb grib
          wgrib2 021_gfs_${date}${hr}f${i}.grb -csv 021_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.0:1:1  -20.5:1:1 022_gfs_${date}${hr}f${i}.grb grib
          wgrib2 022_gfs_${date}${hr}f${i}.grb -csv 022_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.5:1:1  -20.75:1:1 023_gfs_${date}${hr}f${i}.grb grib
          wgrib2 023_gfs_${date}${hr}f${i}.grb -csv 023_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.0:1:1  -21.25:1:1 024_gfs_${date}${hr}f${i}.grb grib
          wgrib2 024_gfs_${date}${hr}f${i}.grb -csv 024_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.5:1:1  -21.25:1:1 025_gfs_${date}${hr}f${i}.grb grib
          wgrib2 025_gfs_${date}${hr}f${i}.grb -csv 025_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -21.25:1:1 026_gfs_${date}${hr}f${i}.grb grib
          wgrib2 026_gfs_${date}${hr}f${i}.grb -csv 026_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.75:1:1  -21.5:1:1 027_gfs_${date}${hr}f${i}.grb grib
          wgrib2 027_gfs_${date}${hr}f${i}.grb -csv 027_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -21.75:1:1 028_gfs_${date}${hr}f${i}.grb grib
          wgrib2 028_gfs_${date}${hr}f${i}.grb -csv 028_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -21.75:1:1 029_gfs_${date}${hr}f${i}.grb grib
          wgrib2 029_gfs_${date}${hr}f${i}.grb -csv 029_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -22.0:1:1 030_gfs_${date}${hr}f${i}.grb grib
          wgrib2 030_gfs_${date}${hr}f${i}.grb -csv 030_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.0:1:1  -22.25:1:1 031_gfs_${date}${hr}f${i}.grb grib
          wgrib2 031_gfs_${date}${hr}f${i}.grb -csv 031_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -22.25:1:1 032_gfs_${date}${hr}f${i}.grb grib
          wgrib2 032_gfs_${date}${hr}f${i}.grb -csv 032_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.75:1:1  -22.25:1:1 033_gfs_${date}${hr}f${i}.grb grib
          wgrib2 033_gfs_${date}${hr}f${i}.grb -csv 033_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.5:1:1  -22.5:1:1 034_gfs_${date}${hr}f${i}.grb grib
          wgrib2 034_gfs_${date}${hr}f${i}.grb -csv 034_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.25:1:1  -22.75:1:1 035_gfs_${date}${hr}f${i}.grb grib
          wgrib2 035_gfs_${date}${hr}f${i}.grb -csv 035_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -22.75:1:1 036_gfs_${date}${hr}f${i}.grb grib
          wgrib2 036_gfs_${date}${hr}f${i}.grb -csv 036_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -22.75:1:1 037_gfs_${date}${hr}f${i}.grb grib
          wgrib2 037_gfs_${date}${hr}f${i}.grb -csv 037_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.0:1:1 038_gfs_${date}${hr}f${i}.grb grib
          wgrib2 038_gfs_${date}${hr}f${i}.grb -csv 038_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -23.0:1:1 039_gfs_${date}${hr}f${i}.grb grib
          wgrib2 039_gfs_${date}${hr}f${i}.grb -csv 039_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.0:1:1 040_gfs_${date}${hr}f${i}.grb grib
          wgrib2 040_gfs_${date}${hr}f${i}.grb -csv 040_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.0:1:1  -23.0:1:1 041_gfs_${date}${hr}f${i}.grb grib
          wgrib2 041_gfs_${date}${hr}f${i}.grb -csv 041_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -43.25:1:1  -23.0:1:1 042_gfs_${date}${hr}f${i}.grb grib
          wgrib2 042_gfs_${date}${hr}f${i}.grb -csv 042_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.25:1:1 043_gfs_${date}${hr}f${i}.grb grib
          wgrib2 043_gfs_${date}${hr}f${i}.grb -csv 043_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -45.75:1:1  -23.25:1:1 044_gfs_${date}${hr}f${i}.grb grib
          wgrib2 044_gfs_${date}${hr}f${i}.grb -csv 044_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.5:1:1 045_gfs_${date}${hr}f${i}.grb grib
          wgrib2 045_gfs_${date}${hr}f${i}.grb -csv 045_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.5:1:1 046_gfs_${date}${hr}f${i}.grb grib
          wgrib2 046_gfs_${date}${hr}f${i}.grb -csv 046_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -23.5:1:1 047_gfs_${date}${hr}f${i}.grb grib
          wgrib2 047_gfs_${date}${hr}f${i}.grb -csv 047_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.25:1:1  -23.5:1:1 048_gfs_${date}${hr}f${i}.grb grib
          wgrib2 048_gfs_${date}${hr}f${i}.grb -csv 048_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -45.5:1:1  -23.5:1:1 049_gfs_${date}${hr}f${i}.grb grib
          wgrib2 049_gfs_${date}${hr}f${i}.grb -csv 049_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.75:1:1 050_gfs_${date}${hr}f${i}.grb grib
          wgrib2 050_gfs_${date}${hr}f${i}.grb -csv 050_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -23.75:1:1 051_gfs_${date}${hr}f${i}.grb grib
          wgrib2 051_gfs_${date}${hr}f${i}.grb -csv 051_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.75:1:1 052_gfs_${date}${hr}f${i}.grb grib
          wgrib2 052_gfs_${date}${hr}f${i}.grb -csv 052_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -23.75:1:1 053_gfs_${date}${hr}f${i}.grb grib
          wgrib2 053_gfs_${date}${hr}f${i}.grb -csv 053_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -24.0:1:1 054_gfs_${date}${hr}f${i}.grb grib
          wgrib2 054_gfs_${date}${hr}f${i}.grb -csv 054_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -24.0:1:1 055_gfs_${date}${hr}f${i}.grb grib
          wgrib2 055_gfs_${date}${hr}f${i}.grb -csv 055_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -24.0:1:1 056_gfs_${date}${hr}f${i}.grb grib
          wgrib2 056_gfs_${date}${hr}f${i}.grb -csv 056_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -24.0:1:1 057_gfs_${date}${hr}f${i}.grb grib
          wgrib2 057_gfs_${date}${hr}f${i}.grb -csv 057_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.25:1:1  -24.0:1:1 058_gfs_${date}${hr}f${i}.grb grib
          wgrib2 058_gfs_${date}${hr}f${i}.grb -csv 058_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.5:1:1  -25.5:1:1 059_gfs_${date}${hr}f${i}.grb grib
          wgrib2 059_gfs_${date}${hr}f${i}.grb -csv 059_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.5:1:1  -25.5:1:1 060_gfs_${date}${hr}f${i}.grb grib
          wgrib2 060_gfs_${date}${hr}f${i}.grb -csv 060_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.25:1:1  -25.5:1:1 061_gfs_${date}${hr}f${i}.grb grib
          wgrib2 061_gfs_${date}${hr}f${i}.grb -csv 061_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -43.0:1:1  -27.0:1:1 062_gfs_${date}${hr}f${i}.grb grib
          wgrib2 062_gfs_${date}${hr}f${i}.grb -csv 062_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -27.25:1:1 063_gfs_${date}${hr}f${i}.grb grib
          wgrib2 063_gfs_${date}${hr}f${i}.grb -csv 063_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.5:1:1  -28.25:1:1 064_gfs_${date}${hr}f${i}.grb grib
          wgrib2 064_gfs_${date}${hr}f${i}.grb -csv 064_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -29.75:1:1 065_gfs_${date}${hr}f${i}.grb grib
          wgrib2 065_gfs_${date}${hr}f${i}.grb -csv 065_gfs_${date}${hr}f${i}.csv &
 
        # add a sleep to prevent a denial of service in case of missing file
        sleep 1
        python3 /var/scripts/noaa_gfs/parser_csv.py &
    done

  
    for i in {252..384..12}; 
        do
            URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
            echo "$URL"
            #download file
            curl "$URL" -o gfs_${date}${hr}f${i}.grb

           wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.0:1:1  -3.25:1:1 001_gfs_${date}${hr}f${i}.grb grib
          wgrib2 001_gfs_${date}${hr}f${i}.grb -csv 001_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.75:1:1  -3.25:1:1 002_gfs_${date}${hr}f${i}.grb grib
          wgrib2 002_gfs_${date}${hr}f${i}.grb -csv 002_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.0:1:1  -3.5:1:1 003_gfs_${date}${hr}f${i}.grb grib
          wgrib2 003_gfs_${date}${hr}f${i}.grb -csv 003_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -35.75:1:1  -9.75:1:1 004_gfs_${date}${hr}f${i}.grb grib
          wgrib2 004_gfs_${date}${hr}f${i}.grb -csv 004_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.5:1:1  -11.75:1:1 005_gfs_${date}${hr}f${i}.grb grib
          wgrib2 005_gfs_${date}${hr}f${i}.grb -csv 005_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -38.5:1:1  -13.0:1:1 006_gfs_${date}${hr}f${i}.grb grib
          wgrib2 006_gfs_${date}${hr}f${i}.grb -csv 006_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.75:1:1  -16.75:1:1 007_gfs_${date}${hr}f${i}.grb grib
          wgrib2 007_gfs_${date}${hr}f${i}.grb -csv 007_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.25:1:1  -17.25:1:1 008_gfs_${date}${hr}f${i}.grb grib
          wgrib2 008_gfs_${date}${hr}f${i}.grb -csv 008_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -53.25:1:1  -17.25:1:1 009_gfs_${date}${hr}f${i}.grb grib
          wgrib2 009_gfs_${date}${hr}f${i}.grb -csv 009_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -53.25:1:1  -17.75:1:1 010_gfs_${date}${hr}f${i}.grb grib
          wgrib2 010_gfs_${date}${hr}f${i}.grb -csv 010_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -17.75:1:1 011_gfs_${date}${hr}f${i}.grb grib
          wgrib2 011_gfs_${date}${hr}f${i}.grb -csv 011_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -17.75:1:1 012_gfs_${date}${hr}f${i}.grb grib
          wgrib2 012_gfs_${date}${hr}f${i}.grb -csv 012_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -18.5:1:1 013_gfs_${date}${hr}f${i}.grb grib
          wgrib2 013_gfs_${date}${hr}f${i}.grb -csv 013_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -18.75:1:1 014_gfs_${date}${hr}f${i}.grb grib
          wgrib2 014_gfs_${date}${hr}f${i}.grb -csv 014_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -18.75:1:1 015_gfs_${date}${hr}f${i}.grb grib
          wgrib2 015_gfs_${date}${hr}f${i}.grb -csv 015_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -19.5:1:1 016_gfs_${date}${hr}f${i}.grb grib
          wgrib2 016_gfs_${date}${hr}f${i}.grb -csv 016_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.75:1:1  -19.75:1:1 017_gfs_${date}${hr}f${i}.grb grib
          wgrib2 017_gfs_${date}${hr}f${i}.grb -csv 017_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.0:1:1  -20.0:1:1 018_gfs_${date}${hr}f${i}.grb grib
          wgrib2 018_gfs_${date}${hr}f${i}.grb -csv 018_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -44.0:1:1  -20.0:1:1 019_gfs_${date}${hr}f${i}.grb grib
          wgrib2 019_gfs_${date}${hr}f${i}.grb -csv 019_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.5:1:1  -20.25:1:1 020_gfs_${date}${hr}f${i}.grb grib
          wgrib2 020_gfs_${date}${hr}f${i}.grb -csv 020_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.25:1:1  -20.25:1:1 021_gfs_${date}${hr}f${i}.grb grib
          wgrib2 021_gfs_${date}${hr}f${i}.grb -csv 021_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.0:1:1  -20.5:1:1 022_gfs_${date}${hr}f${i}.grb grib
          wgrib2 022_gfs_${date}${hr}f${i}.grb -csv 022_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.5:1:1  -20.75:1:1 023_gfs_${date}${hr}f${i}.grb grib
          wgrib2 023_gfs_${date}${hr}f${i}.grb -csv 023_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.0:1:1  -21.25:1:1 024_gfs_${date}${hr}f${i}.grb grib
          wgrib2 024_gfs_${date}${hr}f${i}.grb -csv 024_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.5:1:1  -21.25:1:1 025_gfs_${date}${hr}f${i}.grb grib
          wgrib2 025_gfs_${date}${hr}f${i}.grb -csv 025_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -21.25:1:1 026_gfs_${date}${hr}f${i}.grb grib
          wgrib2 026_gfs_${date}${hr}f${i}.grb -csv 026_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.75:1:1  -21.5:1:1 027_gfs_${date}${hr}f${i}.grb grib
          wgrib2 027_gfs_${date}${hr}f${i}.grb -csv 027_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.25:1:1  -21.75:1:1 028_gfs_${date}${hr}f${i}.grb grib
          wgrib2 028_gfs_${date}${hr}f${i}.grb -csv 028_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -21.75:1:1 029_gfs_${date}${hr}f${i}.grb grib
          wgrib2 029_gfs_${date}${hr}f${i}.grb -csv 029_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -22.0:1:1 030_gfs_${date}${hr}f${i}.grb grib
          wgrib2 030_gfs_${date}${hr}f${i}.grb -csv 030_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.0:1:1  -22.25:1:1 031_gfs_${date}${hr}f${i}.grb grib
          wgrib2 031_gfs_${date}${hr}f${i}.grb -csv 031_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -48.0:1:1  -22.25:1:1 032_gfs_${date}${hr}f${i}.grb grib
          wgrib2 032_gfs_${date}${hr}f${i}.grb -csv 032_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.75:1:1  -22.25:1:1 033_gfs_${date}${hr}f${i}.grb grib
          wgrib2 033_gfs_${date}${hr}f${i}.grb -csv 033_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.5:1:1  -22.5:1:1 034_gfs_${date}${hr}f${i}.grb grib
          wgrib2 034_gfs_${date}${hr}f${i}.grb -csv 034_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -50.25:1:1  -22.75:1:1 035_gfs_${date}${hr}f${i}.grb grib
          wgrib2 035_gfs_${date}${hr}f${i}.grb -csv 035_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -22.75:1:1 036_gfs_${date}${hr}f${i}.grb grib
          wgrib2 036_gfs_${date}${hr}f${i}.grb -csv 036_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -22.75:1:1 037_gfs_${date}${hr}f${i}.grb grib
          wgrib2 037_gfs_${date}${hr}f${i}.grb -csv 037_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.0:1:1 038_gfs_${date}${hr}f${i}.grb grib
          wgrib2 038_gfs_${date}${hr}f${i}.grb -csv 038_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -23.0:1:1 039_gfs_${date}${hr}f${i}.grb grib
          wgrib2 039_gfs_${date}${hr}f${i}.grb -csv 039_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.0:1:1 040_gfs_${date}${hr}f${i}.grb grib
          wgrib2 040_gfs_${date}${hr}f${i}.grb -csv 040_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.0:1:1  -23.0:1:1 041_gfs_${date}${hr}f${i}.grb grib
          wgrib2 041_gfs_${date}${hr}f${i}.grb -csv 041_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -43.25:1:1  -23.0:1:1 042_gfs_${date}${hr}f${i}.grb grib
          wgrib2 042_gfs_${date}${hr}f${i}.grb -csv 042_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.25:1:1 043_gfs_${date}${hr}f${i}.grb grib
          wgrib2 043_gfs_${date}${hr}f${i}.grb -csv 043_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -45.75:1:1  -23.25:1:1 044_gfs_${date}${hr}f${i}.grb grib
          wgrib2 044_gfs_${date}${hr}f${i}.grb -csv 044_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.5:1:1 045_gfs_${date}${hr}f${i}.grb grib
          wgrib2 045_gfs_${date}${hr}f${i}.grb -csv 045_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.5:1:1 046_gfs_${date}${hr}f${i}.grb grib
          wgrib2 046_gfs_${date}${hr}f${i}.grb -csv 046_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -23.5:1:1 047_gfs_${date}${hr}f${i}.grb grib
          wgrib2 047_gfs_${date}${hr}f${i}.grb -csv 047_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.25:1:1  -23.5:1:1 048_gfs_${date}${hr}f${i}.grb grib
          wgrib2 048_gfs_${date}${hr}f${i}.grb -csv 048_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -45.5:1:1  -23.5:1:1 049_gfs_${date}${hr}f${i}.grb grib
          wgrib2 049_gfs_${date}${hr}f${i}.grb -csv 049_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -23.75:1:1 050_gfs_${date}${hr}f${i}.grb grib
          wgrib2 050_gfs_${date}${hr}f${i}.grb -csv 050_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -23.75:1:1 051_gfs_${date}${hr}f${i}.grb grib
          wgrib2 051_gfs_${date}${hr}f${i}.grb -csv 051_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -23.75:1:1 052_gfs_${date}${hr}f${i}.grb grib
          wgrib2 052_gfs_${date}${hr}f${i}.grb -csv 052_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -23.75:1:1 053_gfs_${date}${hr}f${i}.grb grib
          wgrib2 053_gfs_${date}${hr}f${i}.grb -csv 053_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.25:1:1  -24.0:1:1 054_gfs_${date}${hr}f${i}.grb grib
          wgrib2 054_gfs_${date}${hr}f${i}.grb -csv 054_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -47.0:1:1  -24.0:1:1 055_gfs_${date}${hr}f${i}.grb grib
          wgrib2 055_gfs_${date}${hr}f${i}.grb -csv 055_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.75:1:1  -24.0:1:1 056_gfs_${date}${hr}f${i}.grb grib
          wgrib2 056_gfs_${date}${hr}f${i}.grb -csv 056_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.5:1:1  -24.0:1:1 057_gfs_${date}${hr}f${i}.grb grib
          wgrib2 057_gfs_${date}${hr}f${i}.grb -csv 057_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -46.25:1:1  -24.0:1:1 058_gfs_${date}${hr}f${i}.grb grib
          wgrib2 058_gfs_${date}${hr}f${i}.grb -csv 058_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -54.5:1:1  -25.5:1:1 059_gfs_${date}${hr}f${i}.grb grib
          wgrib2 059_gfs_${date}${hr}f${i}.grb -csv 059_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.5:1:1  -25.5:1:1 060_gfs_${date}${hr}f${i}.grb grib
          wgrib2 060_gfs_${date}${hr}f${i}.grb -csv 060_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -49.25:1:1  -25.5:1:1 061_gfs_${date}${hr}f${i}.grb grib
          wgrib2 061_gfs_${date}${hr}f${i}.grb -csv 061_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -43.0:1:1  -27.0:1:1 062_gfs_${date}${hr}f${i}.grb grib
          wgrib2 062_gfs_${date}${hr}f${i}.grb -csv 062_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -27.25:1:1 063_gfs_${date}${hr}f${i}.grb grib
          wgrib2 063_gfs_${date}${hr}f${i}.grb -csv 063_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -52.5:1:1  -28.25:1:1 064_gfs_${date}${hr}f${i}.grb grib
          wgrib2 064_gfs_${date}${hr}f${i}.grb -csv 064_gfs_${date}${hr}f${i}.csv &
 
       wgrib2 gfs_${date}${hr}f${i}.grb -match "surface" -lola  -51.25:1:1  -29.75:1:1 065_gfs_${date}${hr}f${i}.grb grib
          wgrib2 065_gfs_${date}${hr}f${i}.grb -csv 065_gfs_${date}${hr}f${i}.csv &
 
        # add a sleep to prevent a denial of service in case of missing file
        sleep 1
        python3 /var/scripts/noaa_gfs/parser_csv.py &
    done

   

    # tar -czf gfs_${date}${hr}.tar.gz *.grb
    rm *.grb
    # #gsutil cp gfs_${date}${hr}.tar.gz gs://pluvion_forecasts/grib_gfs
    # gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 gfs_${date}${hr}.tar.gz
    # rm gfs_${date}${hr}.tar.gz
    