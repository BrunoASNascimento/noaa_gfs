from publisher_pub_sub import parser_publisher_pubsub
import pandas as pd
import time
from google.cloud import pubsub_v1
import json
import os
from datetime import datetime


directory = '/var/data/gfs/backup/'

project_id = 'pluvion-tech'
topic_name = 'bq_persister'
dataset = 'forecast'
name_table = 'fc_noaa_gfs_16d'


def read_directory():
    list_file = os.listdir(directory)
    # list_file = sorted(list_file, reverse=True)
    while len(list_file) > 0:
        try:
            for file in list_file:
                if file.endswith(".csv"):
                    print("Start: ", file, ' - ', str(datetime.utcnow()))
                    df = pd.read_csv(directory+file)
                    df = df.drop_duplicates()
                    print(parser_publisher_pubsub(df, dataset, name_table,
                                                  project_id, topic_name))
                    print("End: ", file, ' - ', str(datetime.utcnow()))
                    os.remove(directory+file)
        except:
            time.sleep(30)
            pass

    return "done"


read_directory()
