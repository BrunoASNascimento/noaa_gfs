#!/bin/bash
#

cd /var/data/gfs/backup/

python3 /var/scripts/noaa_gfs/persister_upload_pubsub_inverse.py &
python3 /var/scripts/noaa_gfs/persister_upload_pubsub.py

for i in *.csv; do
    echo ${i} &
    #bq --location=US load  --noreplace --source_format=CSV forecast.fc_noaa_gfs_16d ${i} /var/scripts/noaa_gfs/schema_gbq.json &
    
    tar -czf csv_gfs_${i}.tar.gz ${i}
    #gsutil cp ${i}.gz gs://pluvion_forecasts/GFS
    gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 csv_gfs_${i}.tar.gz
    rm ${i}* &
    rm csv_gfs_${i}.tar.gz
done
