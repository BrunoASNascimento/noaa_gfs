import pandas as pd
import time
from google.cloud import pubsub_v1
import json
import os
from datetime import datetime
import time

directory = '/var/data/gfs/backup/'

project_id = 'pluvion-tech'
topic_name = 'data_to_gbq'
name_table = 'forecast.fc_noaa_gfs_16d'

step = 50000


def read_directory():
    list_file = os.listdir(directory)
    list_file = sorted(list_file)

    for file in list_file[0:int((len(list_file)+1)/2)]:
        if file.endswith(".csv"):
            #print("Start: ", file, ' - ', str(datetime.utcnow()))
            # publisher_pubsub(directory+file)
            #print("End: ", file, ' - ', str(datetime.utcnow()))
            try:
                print("Start: ", file, ' - ', str(datetime.utcnow()))
                publisher_pubsub(directory+file)
                print("End: ", file, ' - ', str(datetime.utcnow()))
            except:
                continue
    return "done"


def callback(message_future):
    if message_future.exception(timeout=30):
        print('Publishing message on {} threw an Exception {}.'.format(
            topic_name, message_future.exception()))
    else:
        print(message_future.result())


def publisher_pubsub(name_file):

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)

    df = pd.read_csv(name_file)
    df = df.drop_duplicates()
    # print(df['date'].count())

    size = df['date'].count()
    print('Published message IDs:')
    control = 0
    for i in range(0, size, step):
        time.sleep(2)
        control = control+1
        # print(size)
        #print(i, i+step)
        df_edit = df[i:i+step]
        # print(df_edit.shape)
        df_edit = df_edit.to_json(orient='split', index=False)

        data = {
            "project_id": project_id,
            "name_table": name_table,
            "data": df_edit,
            "to_date": ['date', 'forecast']
        }
        #data = str(data).replace("'", '"')
        data = json.dumps(data)
        data = data.encode('utf-8')
        message_future = publisher.publish(topic_path, data=data)
        message_future.add_done_callback(callback)
        if(control % 10 == 0):
            time.sleep(300)


    # print(data)
read_directory()
