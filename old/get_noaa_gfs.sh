#!/bin/bash
#
#define URL
#


cd /var/data/gfs

fhr=0p25
date=$(date +'%Y%m%d')

hr=00
for i in {001..120..1}; do
    URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
    echo "$URL"
    #download file
    curl "$URL" -o gfs_${date}${hr}f${i}.grb
    #Parser
    wgrib2 -match "surface" ./gfs_${date}${hr}f${i}.grb -csv /var/data/gfs/gfs_${date}${hr}f${i}.csv
    # add a sleep to prevent a denial of service in case of missing file
    #gdrive upload --parent 1reCjIqkt6_TUcQmbVa_zMBqCkcEK9ZQS ./gfs_${date}${hr}f${i}.grb
    #rm gfs_${date}${hr}f${i}.grb
    sleep 1
done

python3 /var/scripts/noaa_gfs/main_normal.py &
python3 /var/scripts/noaa_gfs/main_reverse.py &


for i in {123..240..3}; do
    URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
    echo "$URL"
    #download file
    curl "$URL" -o gfs_${date}${hr}f${i}.grb
    #Parser
    wgrib2 -match "surface" ./gfs_${date}${hr}f${i}.grb -csv /var/data/gfs/gfs_${date}${hr}f${i}.csv
    # add a sleep to prevent a denial of service in case of missing file
    #gdrive upload --parent 1reCjIqkt6_TUcQmbVa_zMBqCkcEK9ZQS ./gfs_${date}${hr}f${i}.grb
    #rm gfs_${date}${hr}f${i}.grb
    sleep 1
done

for i in {252..384..12}; do
    URL="https://nomads.ncep.noaa.gov/cgi-bin/filter_gfs_0p25.pl?file=gfs.t${hr}z.pgrb2.${fhr}.f$i&all_lev=on&all_var=on&subregion=&leftlon=-74&rightlon=-34.465&toplat=5.1&bottomlat=-34.3&dir=%2Fgfs.${date}%2F${hr}"
    echo "$URL"
    #download file
    curl "$URL" -o gfs_${date}${hr}f${i}.grb
    #Parser
    wgrib2 -match "surface" ./gfs_${date}${hr}f${i}.grb -csv /var/data/gfs/gfs_${date}${hr}f${i}.csv
    # add a sleep to prevent a denial of service in case of missing file
    #gdrive upload --parent 1reCjIqkt6_TUcQmbVa_zMBqCkcEK9ZQS ./gfs_${date}${hr}f${i}.grb
    #rm gfs_${date}${hr}f${i}.grb
    sleep 1
done

python3 /var/scripts/noaa_gfs/main_normal.py &
python3 /var/scripts/noaa_gfs/main_reverse.py &

tar -czf gfs_${date}${hr}.tar.gz *.grb
rm *.grb &
#gsutil cp gfs_${date}${hr}.tar.gz gs://pluvion_forecasts/grib_gfs
gdrive upload --parent 1EQY76y_rM1vMffuEJDNs2RruJtbK-4i3 gfs_${date}${hr}.tar.gz
rm gfs_${date}${hr}.tar.gz
