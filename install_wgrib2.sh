#!/bin/bash

APP_DIR="/opt/wgrib2";
sudo mkdir -p "$APP_DIR";
sudo chown ${USER} --recursive "$APP_DIR";

cd "$APP_DIR";

wget ftp://ftp.cpc.ncep.noaa.gov/wd51we/wgrib2/wgrib2.tgz;

tar -xzvf wgrib2.tgz

cd grib2

sudo apt-get update
sudo apt-get install -y gcc gfortran make gfortran-7 libnetcdf-dev libnetcdff-dev;


export CC=gcc
export FC=gfortran
make

echo "export PATH=$PATH:$APP_DIR/grib2/wgrib2" >> ~/.bashrc

sleep 5
source ~/.bashrc
sleep 10
wgrib2 -config
