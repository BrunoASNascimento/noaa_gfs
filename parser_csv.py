import pandas as pd
import os
import datetime
import time
from google.cloud import pubsub_v1
import json
from publisher_pub_sub import parser_publisher_pubsub


directory = "/var/data/gfs/"
# directory = "X:/Development/gitlab.com/pluvion-forecasts/noaa_gfs/test/"
project_id = 'pluvion-tech'
topic_name = 'bq_persister'
dataset = 'forecast'
name_table = 'fc_noaa_gfs_16d'


def read_directory():
    list_file = os.listdir(directory)
    columns = ['date', 'forecast', 'data_type',
               'surface', 'lon', 'lat', 'data']
    list_file = sorted(list_file)
    for file in list_file:
        if file.endswith(".csv"):
            print(file, ' - ', str(datetime.datetime.utcnow()))
            df = pd.read_csv(directory+file,
                             header=None, names=columns)
            clean_data(df.drop_duplicates(), file)
            os.remove(directory+file)
            try:
                os.remove(directory+file.replace(".csv", ".grb"))
            except:
                pass


def clean_data(df, file_name):
    variables = ['date', 'forecast', 'surface', 'lon', 'lat',
                 'ACPCP', 'APCP', 'CPRAT', 'PRATE', 'PRES', 'TMP', 'HGT']

    df_edit = df[['date', 'forecast', 'surface',
                  'lon', 'lat']].drop_duplicates()
    df_edit = df_edit[df_edit['surface'] == 'surface'].drop_duplicates()
    df_ACPCP = df[df['data_type'] == 'ACPCP']
    df_ACPCP.rename(columns={'data': 'ACPCP'}, inplace=True)
    result = pd.merge(df_edit, df_ACPCP, on=variables[0:5])
    result = result[variables[0:6]]
    df_ACPCP = []

    for i in range(6, len(variables)):
        name = str(variables[i])
        print("Parser: ", name)
        df_log = df[df['data_type'] == name]
        df_log.rename(columns={'data': name}, inplace=True)
        result = pd.merge(result, df_log, on=variables[0:5])
        result = result[variables[0:i+1]]
        df_log = []
        # print(result)

    result['date'] = pd.to_datetime(
        result['date'], format='%Y-%m-%d %H:%M:%S')
    result['forecast'] = pd.to_datetime(
        result['forecast'], format='%Y-%m-%d %H:%M:%S')
    result.drop_duplicates()
    print('save backup')
    result.to_csv(directory+'backup/parsed_'+file_name, index=False)

    parser_publisher_pubsub(result, dataset, name_table,
                            project_id, topic_name)


read_directory()
