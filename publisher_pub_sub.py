import pandas as pd
from google.cloud import pubsub_v1
import json
from datetime import datetime


def callback(message_future):
    if message_future.exception(timeout=30):
        print('Publishing message on topic_name threw an Exception {}.'.format(
            message_future.exception()))
    else:
        print(message_future.result())


def publisher_pubsub(data_json, project_id, topic_name):
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)
    data = json.dumps(data_json)
    data = data.encode('utf-8')
    message_future = publisher.publish(topic_path, data=data)
    message_future.add_done_callback(callback)


def parser_publisher_pubsub(data, dataset, name_table, project_id, topic_name):
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_name)

    if data.shape[0] > 0:
        df_edit = json.loads(data.to_json(orient='table', index=False))

        data_json = {
            "dataset": dataset,
            "table": name_table,
            "data": []
        }

        for df_row in df_edit['data']:
            data_json['data'].append({
                "objId": {
                    "field": "msg_parser_"+name_table,
                    "value": "msg_parser_"+name_table+datetime.strftime(datetime.utcnow(), '%S%f'),
                },
                "obj": df_row
            })

            if len(data_json['data']) == 500:
                publisher_pubsub(data_json, project_id, topic_name)
                data_json['data'] = []

        publisher_pubsub(data_json, project_id, topic_name)

    return 'Upload OK'
